package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.ruble;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class HundredRubleHandler extends RubleHandlerBase {

    public HundredRubleHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected int value = 100;

    @Override
    protected int getValue() {
        return value;
    }
}
