package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class FiveHundredEuroHandler extends EuroHandlerBase {

    protected int value = 500;

    public FiveHundredEuroHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }
}
