package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Bancomat bancomat = new Bancomat();

        System.out.println(bancomat.validate("100 USD")); // true
        System.out.println(bancomat.validate("105 USD")); // false
        //
//        System.out.println(bancomat.validate("100 Dollars")); // IAE: Incorrect currency type

        Scanner scanner = new Scanner(System.in);
        String value = scanner.nextLine();

        System.out.println(bancomat.cashe(value));
    }
}
