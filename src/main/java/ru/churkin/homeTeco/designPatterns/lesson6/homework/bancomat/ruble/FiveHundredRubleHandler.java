package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.ruble;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class FiveHundredRubleHandler extends RubleHandlerBase {

    public FiveHundredRubleHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected int value = 500;

    @Override
    protected int getValue() {
        return value;
    }
}
