package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.banknote.CurrencyType;

import java.util.Arrays;

/**
 * Хэндлер проверяет корректность введенной валюты (USD, EUR, RUB)
 * и значение запрашиваемой суммы (не должно превышать 10_000 долл,
 * 20_000 евро или 100_000 рублей).
 * При несоответствии пробразывается исключение.
 */
public class InputHandler extends CommonHandler {

    public InputHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public boolean validate(String banknote) {
        if (Arrays.stream(CurrencyType.values()).noneMatch(e-> e.name().equals(banknote.split(" ")[1]))) {
            throw new IllegalArgumentException("Incorrect currency type");
        }
        return super.validate(banknote);
    }

    @Override
    public String cash(String amount) {
        String[] arr = amount.split(" ");
        return this.cash(arr[0], arr[1], new StringBuilder());
    }

    @Override
    public String cash(String amount, String currency, StringBuilder cache) {
        if (Arrays.stream(CurrencyType.values()).noneMatch(e-> e.name().equals(currency))) {
           throw new IllegalArgumentException("Incorrect currency type");
        }
        if (CurrencyType.EUR.name().equals(currency) && Integer.parseInt(amount) > 10_000){
            throw new IllegalArgumentException("Too much amount in EURO");
        }
        if (CurrencyType.USD.name().equals(currency) && Integer.parseInt(amount) > 20_000){
            throw new IllegalArgumentException("Too much amount in US Dollars");
        }
        if (CurrencyType.RUB.name().equals(currency) && Integer.parseInt(amount) > 100_000){
            throw new IllegalArgumentException("Too much amount in Rubles");
        }
        return super.cash(amount, currency, cache);
    }
}
