package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class HundredEuroHandler extends EuroHandlerBase {

    protected int value = 100;

    public HundredEuroHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }
}
