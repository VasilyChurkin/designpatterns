package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.ruble;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class FiveThousandRubleHandler extends RubleHandlerBase {

    public FiveThousandRubleHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected int value = 5000;

    @Override
    protected int getValue() {
        return value;
    }
}
