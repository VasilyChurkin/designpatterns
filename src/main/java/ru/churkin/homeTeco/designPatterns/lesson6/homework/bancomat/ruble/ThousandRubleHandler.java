package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.ruble;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class ThousandRubleHandler extends RubleHandlerBase {

    public ThousandRubleHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected int value = 1000;

    @Override
    protected int getValue() {
        return value;
    }
}
