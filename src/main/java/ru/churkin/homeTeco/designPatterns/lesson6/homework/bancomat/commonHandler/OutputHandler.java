package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler;

public class OutputHandler extends CommonHandler {

    public OutputHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public String cash(String amount, String currency, StringBuilder cache) {
        if (!amount.equals("0")) {
            System.out.print("Невозможно выдать запрашиваемую сумму");
            cache = new StringBuilder();
        }
        return super.cash(amount, currency, cache);
    }
}
