package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.dollar;


import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.BanknoteHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.banknote.CurrencyType;

public abstract class DollarHandlerBase extends BanknoteHandler {
    protected DollarHandlerBase(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected CurrencyType currencyType = CurrencyType.USD;

    @Override
    protected CurrencyType getCurrency() {
        return currencyType;
    }
}
