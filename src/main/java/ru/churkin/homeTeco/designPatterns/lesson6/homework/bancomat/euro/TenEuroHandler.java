package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class TenEuroHandler extends EuroHandlerBase {

    public TenEuroHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected int value = 10;

    @Override
    protected int getValue() {
        return value;
    }
}
