package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.InputHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.OutputHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.dollar.FiftyDollarHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.dollar.HundredDollarHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.dollar.TenDollarHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro.FiftyEuroHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro.FiveHundredEuroHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro.HundredEuroHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro.TenEuroHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.ruble.*;


public class Bancomat {
    private CommonHandler handler;

    public Bancomat() {
        handler = new OutputHandler(null);

        handler = new HundredRubleHandler(handler);
        handler = new FiveHundredRubleHandler(handler);
        handler = new ThousandRubleHandler(handler);
        handler = new FiveThousandRubleHandler(handler);

        handler = new TenDollarHandler(handler);
        handler = new FiftyDollarHandler(handler);
        handler = new HundredDollarHandler(handler);

        handler = new TenEuroHandler(handler);
        handler = new FiftyEuroHandler(handler);
        handler = new HundredEuroHandler(handler);
        handler = new FiveHundredEuroHandler(handler);

        handler = new InputHandler(handler);

    }

    public boolean validate(String banknote) {
        return handler.validate(banknote);
    }

    public String cashe(String amount) {
        return handler.cash(amount);
    }

}
