package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.ruble;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.banknote.CurrencyType;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.BanknoteHandler;
import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public abstract class RubleHandlerBase extends BanknoteHandler {

    protected RubleHandlerBase(CommonHandler nextHandler) {
        super(nextHandler);
    }

    protected CurrencyType currencyType = CurrencyType.RUB;

    @Override
    protected CurrencyType getCurrency() {
        return currencyType;
    }
}
