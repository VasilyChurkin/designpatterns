package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.banknote;

public enum CurrencyType {
    EUR,
    USD,
    RUB
}
