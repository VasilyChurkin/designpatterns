package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.euro;

import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class FiftyEuroHandler extends EuroHandlerBase {

    protected int value = 50;

    public FiftyEuroHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }
}
