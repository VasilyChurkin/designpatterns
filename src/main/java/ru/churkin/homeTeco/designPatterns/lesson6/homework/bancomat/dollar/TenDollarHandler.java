package ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.dollar;


import ru.churkin.homeTeco.designPatterns.lesson6.homework.bancomat.commonHandler.CommonHandler;

public class TenDollarHandler extends DollarHandlerBase {

    protected int value = 10;

    public TenDollarHandler(CommonHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }
}
