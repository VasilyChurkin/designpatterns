package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.audi;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Brand;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;

public class AudiBody extends Body {
    public AudiBody(String name, Color color, BodyStyle bodyStyle) {
        super(name, color, bodyStyle, Brand.AUDI);
    }
}
