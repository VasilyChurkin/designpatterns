package ru.churkin.homeTeco.designPatterns.lesson2.homework.model;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public abstract class Car {

    private Body body;
    private Engine engine;

    public Car(Body body, Engine engine) {
        this.body = body;
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Car{" +
                "body=" + body +
                ", engine=" + engine +
                '}';
    }
}
