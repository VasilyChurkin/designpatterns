package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.bmw;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.CarFactory;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class BmwFactory extends CarFactory {

    private BmwFactory() {
    }

    static class Holder {
        public static BmwFactory instance = new BmwFactory();
    }

    public static BmwFactory getInstance() {
        return Holder.instance;
    }

    @Override
    public Body createBody(String name, Color color, BodyStyle bodyStyle) {
        return new BmwBody(name, color, bodyStyle);
    }

    @Override
    public Engine createEngine(Double power, Fuel fuel, Double volume) {
        return new BmwEngine(power, fuel, volume);
    }

    @Override
    public Car createCar(Body body, Engine engine) {
        return new BmwCar(body, engine);
    }
}
