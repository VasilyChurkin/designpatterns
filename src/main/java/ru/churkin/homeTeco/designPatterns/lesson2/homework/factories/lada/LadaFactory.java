package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.lada;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.CarFactory;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class LadaFactory extends CarFactory {

    private LadaFactory() {
    }

    static class Holder {
        static LadaFactory instance = new LadaFactory();
    }

    public static LadaFactory getInstance() {
        return Holder.instance;
    }

    @Override
    public Body createBody(String name, Color color, BodyStyle bodyStyle) {
        return new LadaBody(name, color, bodyStyle);
    }

    @Override
    public Engine createEngine(Double power, Fuel fuel, Double volume) {
        return new LadaEngine(power, fuel, volume);
    }

    @Override
    public Car createCar(Body body, Engine engine) {
        return new LadaCar(body, engine);
    }

}
