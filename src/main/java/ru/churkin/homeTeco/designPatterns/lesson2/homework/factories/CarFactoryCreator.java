package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Brand;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.audi.AudiFactory;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.bmw.BmwFactory;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.lada.LadaFactory;

public class CarFactoryCreator {

    public static CarFactory createCarFactory(Brand brand) {
        switch (brand) {
            case LADA:
                return LadaFactory.getInstance();
            case BMW:
                return BmwFactory.getInstance();
            case AUDI:
                return AudiFactory.getInstance();
            default:
                throw new RuntimeException("No such car brand");
        }

    }
}
