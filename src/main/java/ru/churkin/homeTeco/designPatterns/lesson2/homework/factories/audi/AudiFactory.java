package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.audi;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.CarFactory;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class AudiFactory extends CarFactory {

    private AudiFactory() {
    }

    private static class Holder {
        static AudiFactory instance = new AudiFactory();
    }

    public static AudiFactory getInstance() {
        return Holder.instance;
    }

    @Override
    public Body createBody(String name, Color color, BodyStyle bodyStyle) {
        return new AudiBody(name, color, bodyStyle);
    }

    @Override
    public Engine createEngine(Double power, Fuel fuel, Double volume) {
        return new AudiEngine(power, fuel, volume);
    }

    @Override
    public Car createCar(Body body, Engine engine) {
        return new AudiCar(body, engine);
    }
}
