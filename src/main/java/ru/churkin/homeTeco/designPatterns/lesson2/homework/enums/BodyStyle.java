package ru.churkin.homeTeco.designPatterns.lesson2.homework.enums;

public enum BodyStyle {

    COUPE,
    HATCHBACK,
    MINIVAN,
    SEDAN
}
