package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.bmw;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class BmwEngine extends Engine {

    public BmwEngine(Double power, Fuel fuel, Double volume) {
        super(power, fuel, volume);
    }
}
