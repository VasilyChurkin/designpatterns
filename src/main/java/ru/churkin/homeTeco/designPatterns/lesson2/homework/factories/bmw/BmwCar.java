package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.bmw;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class BmwCar extends Car {
    public BmwCar(Body body, Engine engine) {
        super(body, engine);
    }
}
