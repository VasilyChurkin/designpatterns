package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.lada;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class LadaEngine extends Engine {

    public LadaEngine(Double power, Fuel fuel, Double volume) {
        super(power, fuel, volume);
    }
}
