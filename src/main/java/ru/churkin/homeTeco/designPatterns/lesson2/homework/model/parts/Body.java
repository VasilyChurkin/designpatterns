package ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Brand;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;

public abstract class Body {

    String name;
    Color color;
    BodyStyle bodyStyle;
    Brand brand;

    public Body(String name, Color color, BodyStyle bodyStyle, Brand brand) {
        this.name = name;
        this.color = color;
        this.bodyStyle = bodyStyle;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Body{" +
                "brand=" + brand +
                ", name='" + name + '\'' +
                ", color=" + color +
                ", bodyStyle=" + bodyStyle +
                '}';
    }
}
