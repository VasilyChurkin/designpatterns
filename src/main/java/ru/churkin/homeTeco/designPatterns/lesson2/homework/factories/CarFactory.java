package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public abstract class CarFactory {

    abstract public Body createBody(String name, Color color, BodyStyle bodyStyle);

    abstract public Engine createEngine(Double power, Fuel fuel, Double volume);

    abstract public Car createCar(Body body, Engine engine);
}
