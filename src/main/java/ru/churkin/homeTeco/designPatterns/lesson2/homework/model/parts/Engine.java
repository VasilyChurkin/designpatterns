package ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;

public abstract class Engine {

    Double power;
    Fuel fuel;
    Double volume;

    public Engine(Double power, Fuel fuel, Double volume) {
        this.power = power;
        this.fuel = fuel;
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "power=" + power +
                ", fuel=" + fuel +
                ", volume=" + volume +
                '}';
    }
}
