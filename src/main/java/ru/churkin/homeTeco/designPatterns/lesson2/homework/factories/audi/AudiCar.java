package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.audi;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class AudiCar extends Car {
    public AudiCar(Body body, Engine engine) {
        super(body, engine);
    }
}
