package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.audi;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class AudiEngine extends Engine {
    public AudiEngine(Double power, Fuel fuel, Double volume) {
        super(power, fuel, volume);
    }
}
