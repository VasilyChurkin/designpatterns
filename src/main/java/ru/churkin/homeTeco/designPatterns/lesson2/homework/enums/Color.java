package ru.churkin.homeTeco.designPatterns.lesson2.homework.enums;

public enum Color {

    BLACK,
    RED,
    WHITE
}
