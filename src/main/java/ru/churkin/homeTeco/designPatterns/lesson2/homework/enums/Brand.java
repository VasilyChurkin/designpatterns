package ru.churkin.homeTeco.designPatterns.lesson2.homework.enums;

public enum Brand {

    AUDI,
    LADA,
    BMW
}
