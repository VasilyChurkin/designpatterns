package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.bmw;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Brand;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;

public class BmwBody extends Body {

    public BmwBody(String name, Color color, BodyStyle bodyStyle) {
        super(name, color, bodyStyle, Brand.BMW);
    }
}
