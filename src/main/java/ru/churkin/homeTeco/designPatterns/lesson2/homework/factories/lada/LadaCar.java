package ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.lada;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class LadaCar extends Car {
    public LadaCar(Body body, Engine engine) {
        super(body, engine);
    }
}
