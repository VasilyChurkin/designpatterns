package ru.churkin.homeTeco.designPatterns.lesson2.homework;

import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.BodyStyle;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Brand;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Color;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.enums.Fuel;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.CarFactory;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.factories.CarFactoryCreator;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.Car;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Body;
import ru.churkin.homeTeco.designPatterns.lesson2.homework.model.parts.Engine;

public class Main {

    public static void main(String[] args) {

        CarFactory ladaFactory = CarFactoryCreator.createCarFactory(Brand.LADA);
        Body body = ladaFactory.createBody("Kalina", Color.BLACK, BodyStyle.HATCHBACK);
        Engine engine = ladaFactory.createEngine(110d, Fuel.DISEL, 1.6d);
        Car kalina = ladaFactory.createCar(body, engine);
        System.out.println(kalina);

        CarFactory bmwFactory = CarFactoryCreator.createCarFactory(Brand.BMW);
        Body bmwBody = bmwFactory.createBody("M2", Color.RED, BodyStyle.COUPE);
        Engine bmwEngine = bmwFactory.createEngine(180d, Fuel.PETROL, 2.2d);
        Car bmw = bmwFactory.createCar(bmwBody, bmwEngine);
        System.out.println(bmw);

        CarFactory audiFactory = CarFactoryCreator.createCarFactory(Brand.AUDI);
        Body audiBody = audiFactory.createBody("A3", Color.WHITE, BodyStyle.SEDAN);
        Engine audiEngine = audiFactory.createEngine(180d, Fuel.PETROL, 2.2d);
        Car audi = audiFactory.createCar(audiBody, audiEngine);
        System.out.println(audi);

    }
}
