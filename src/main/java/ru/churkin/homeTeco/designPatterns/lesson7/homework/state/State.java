package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;

public interface State {

    void getCache(DeviceContext context);
    void changeInputDevice(DeviceContext context);
    void changeDoc(DeviceContext context);
    void print(DeviceContext context, boolean repeat);
    void giveChange(DeviceContext context);
}
