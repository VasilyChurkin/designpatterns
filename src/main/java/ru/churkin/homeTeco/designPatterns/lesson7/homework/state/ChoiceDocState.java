package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;

/**
 * 3 шаг: Выбор документа
 */
public class ChoiceDocState extends BaseState {
    @Override
    public void changeDoc(DeviceContext context) {
        System.out.println("Document changed");
        context.setState(new PrintDocumentState());
    }
}
