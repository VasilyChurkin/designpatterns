package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;

/**
 * 2 шаг: выбор устройства, которое содержит информацию
 */
public class ChoiceDeviceState extends BaseState {
    @Override
    public void changeInputDevice(DeviceContext context) {
        System.out.println("Input Device changed");
        context.setState(new ChoiceDocState());
    }
}
