package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;

/**
 * Первый шаг: взять деньги
 */
public class TakeCacheState extends BaseState {
    @Override
    public void getCache(DeviceContext context) {
        System.out.println("Input your money");
        context.setState(new ChoiceDeviceState());
    }
}
