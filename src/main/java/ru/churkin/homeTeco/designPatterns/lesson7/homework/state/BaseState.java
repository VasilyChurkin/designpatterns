package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;
import ru.churkin.homeTeco.designPatterns.lesson7.homework.exception.PrintDeviceException;

public abstract class BaseState implements State {
    @Override
    public void getCache(DeviceContext context){
    }

    @Override
    public void changeInputDevice(DeviceContext context) {
        throw new PrintDeviceException("Pay money first");
    }

    @Override
    public void changeDoc(DeviceContext context) {
        throw new  PrintDeviceException("Choose device first");
    }

    @Override
    public void print(DeviceContext context, boolean repeat) {
        throw new  PrintDeviceException("Choose document first");
    }

    @Override
    public void giveChange(DeviceContext context) {
        throw new PrintDeviceException("Take your document first");
    }
}
