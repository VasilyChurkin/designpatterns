package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;
import ru.churkin.homeTeco.designPatterns.lesson7.homework.exception.PrintDeviceException;

/**
 * Финальный шаг.
 */
public class FinalState extends BaseState {
    @Override
    public void getCache(DeviceContext context) {
        throw new PrintDeviceException("You finish print doc. You finish print doc. Good bye and come again");
    }

    @Override
    public void changeInputDevice(DeviceContext context) {
        throw new PrintDeviceException("You finish print doc. Good bye and come again");
    }

    @Override
    public void changeDoc(DeviceContext context) {
        throw new PrintDeviceException("You finish print doc. Good bye and come again");
    }

    @Override
    public void print(DeviceContext context, boolean repeat) {
        throw new PrintDeviceException("You finish print doc. Good bye and come again");
    }

    @Override
    public void giveChange(DeviceContext context) {
        throw new PrintDeviceException("You finish print doc. Good bye and come again");
    }
}
