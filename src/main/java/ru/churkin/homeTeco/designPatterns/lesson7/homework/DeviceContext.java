package ru.churkin.homeTeco.designPatterns.lesson7.homework;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.state.TakeCacheState;
import ru.churkin.homeTeco.designPatterns.lesson7.homework.state.State;

public class DeviceContext {

    private State state;
    private String act;

    public DeviceContext(String act) {
        this.act = act;
        this.state = new TakeCacheState();
    }

    public void getCache(){
        state.getCache(this);
    }

    public void changeInputDevice() {
        state.changeInputDevice(this);
    }

    public void changeDoc() {
        state.changeDoc(this);
    }

    public void print(boolean repeat) {
        state.print(this, repeat);
    }

    public void giveChange() {
        state.giveChange(this);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }
}
