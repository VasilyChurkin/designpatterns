package ru.churkin.homeTeco.designPatterns.lesson7.homework;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.exception.PrintDeviceException;

public class Main {

    public static void main(String[] args) {
        /**
         * Позитивный сценарий, все идет по плану
         */
//        DeviceContext context = new DeviceContext("123");
//        context.getCache();
//        context.changeInputDevice();
//        context.changeDoc();
//        context.print(false);
//        context.giveChange();

        /**
         * Негативный сценарий: печать до внесения денег
         */
//        DeviceContext context1 = new DeviceContext("1");
//        try {
//            context1.print();
//        }
//        catch (PrintDeviceException e) {
//            System.out.println(e.getMessage());
//        }

        /**
         * Позитивный сценарий: печать документа с повтором
         */
        DeviceContext context2 = new DeviceContext("2");
        try {
            context2.getCache();
            context2.changeInputDevice();
            context2.changeDoc();
            context2.print(true);
            context2.changeDoc();
            context2.print(false);
            context2.giveChange();
        }
        catch (PrintDeviceException e) {
            System.out.println(e.getMessage());
        }
    }

}
