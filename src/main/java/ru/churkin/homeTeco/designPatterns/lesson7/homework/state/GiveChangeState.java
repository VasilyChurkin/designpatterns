package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;

/**
 * 6 шаг: выдаем клиенту сдачу
 */
public class GiveChangeState extends BaseState {
    @Override
    public void giveChange(DeviceContext context) {
        System.out.println("Take your change");
        context.setState(new FinalState());
    }
}
