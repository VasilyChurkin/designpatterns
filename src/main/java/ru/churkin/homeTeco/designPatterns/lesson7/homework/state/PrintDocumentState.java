package ru.churkin.homeTeco.designPatterns.lesson7.homework.state;

import ru.churkin.homeTeco.designPatterns.lesson7.homework.DeviceContext;

/**
 * 4 шаг: печатаем документ
 */
public class PrintDocumentState extends BaseState {

    @Override
    public void print(DeviceContext context, boolean repeat) {
        System.out.println("Printing document ...");
        // можно было считывать с консоли, а не заводить в аргумент метода
        context.setState(new ChoiceDocState());
        if (!repeat) {
            context.setState(new GiveChangeState());
        }
    }
}
