package ru.churkin.homeTeco.designPatterns.lesson7.homework;
/**
Необходимо реализовать поведение для копировального автомата с помощью паттерна State

        Алгоритм работы с копировальным автоматом:
        1. Вносим деньги
        2. Выбираем устройство, которое содержит информацию (флешка или wi-fi устройство)
        3. Выбираем документ
        4. Печатаем документ
        5. Если нужно ещё распечатать документ, возвращаетмся к п.3
        6. Забираем сдачу
*/