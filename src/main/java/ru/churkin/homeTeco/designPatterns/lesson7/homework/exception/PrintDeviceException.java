package ru.churkin.homeTeco.designPatterns.lesson7.homework.exception;

public class PrintDeviceException extends RuntimeException {
    public PrintDeviceException(String message) {
        super(message);
    }
}
