package ru.churkin.homeTeco.designPatterns.lesson4.homework.decorators;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.MessageManager;
import ru.churkin.homeTeco.designPatterns.lesson4.homework.model.Message;

public class UserNameDecorator extends MessageDecoratorBase {

    public UserNameDecorator(MessageManager messageManager) {
        super(messageManager);
    }

    @Override
    protected Message beforeSend(Message message) {
        message.setAuthor(encodeAuthor(message.getAuthor()));
        message.setReceiver(encodeReceiver(message.getReceiver()));
        return message;
    }

    @Override
    protected Message afterReceive(Message message) {
        message.setReceiver(decodeReceiver(message.getReceiver()));
        message.setAuthor(decodeAuthor(message.getAuthor()));
        return message;
    }

    private String encodeReceiver(String receiver) {
        return "#EncodeReceiver::" + receiver;
    }

    private String encodeAuthor(String author) {
        return "#EncodeAuthor::" + author;
    }

    private String decodeReceiver(String receiver) {
        final String CODE = "#CryptReceiver::";
        if (receiver.startsWith(CODE)) {
            return receiver.substring(CODE.length());
        }
        return "Incorrect receiver format";
    }

    private String decodeAuthor(String author) {
        final String CODE = "#CryptAuthor::";
        if (author.startsWith(CODE)) {
            return author.substring(CODE.length());
        }
        return "Incorrect author format";
    }


}
