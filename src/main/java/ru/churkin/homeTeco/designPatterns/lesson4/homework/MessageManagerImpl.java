package ru.churkin.homeTeco.designPatterns.lesson4.homework;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.model.Message;

public class MessageManagerImpl implements MessageManager {
    @Override
    public void sendMessage(Message message) {
        System.out.println("Send message:" + message);
    }

    @Override
    public Message receiveMessage(Message message) {
        System.out.println("Receive message: " + message);
        return message;
    }
}
