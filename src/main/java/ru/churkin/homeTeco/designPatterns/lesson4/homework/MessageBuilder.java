package ru.churkin.homeTeco.designPatterns.lesson4.homework;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.decorators.BodyDecorator;
import ru.churkin.homeTeco.designPatterns.lesson4.homework.decorators.UserNameDecorator;

public class MessageBuilder {

    private MessageManager messageManager;

    public MessageBuilder(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    public MessageBuilder encodeUsers() {
        messageManager = new UserNameDecorator(messageManager);
        return this;
    }

    public MessageBuilder encodeBody() {
        messageManager = new BodyDecorator(messageManager);
        return this;
    }

    public MessageManager build() {
        return messageManager;
    }
}
