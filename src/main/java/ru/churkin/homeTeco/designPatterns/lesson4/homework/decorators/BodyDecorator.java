package ru.churkin.homeTeco.designPatterns.lesson4.homework.decorators;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.MessageManager;
import ru.churkin.homeTeco.designPatterns.lesson4.homework.model.Message;

public class BodyDecorator extends MessageDecoratorBase {

    public BodyDecorator(MessageManager manager) {
        super(manager);
    }

    @Override
    protected Message beforeSend(Message message) {
        message.setText(encodeText(message.getText()));
        return message;
    }

    @Override
    protected Message afterReceive(Message message) {
        message.setText(decodeText(message.getText()));
        return super.afterReceive(message);
    }

    private String encodeText(String text) {
        return "#::" + text + "::#";
    }

    private String decodeText(String text) {
        if (text.startsWith("#::") && text.endsWith("::#")) {
            return text.substring(3, text.length() - 3);
        }
        return "Incorrect text format";
    }

}
