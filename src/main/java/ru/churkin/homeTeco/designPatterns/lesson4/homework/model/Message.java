package ru.churkin.homeTeco.designPatterns.lesson4.homework.model;

public class Message {

    private String author;
    private String receiver;
    private String text;

    public Message() {
    }

    public Message(String author, String receiver, String text) {
        this.author = author;
        this.receiver = receiver;
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "author='" + author + '\'' +
                ", receiver='" + receiver + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
