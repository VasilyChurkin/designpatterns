package ru.churkin.homeTeco.designPatterns.lesson4.homework.decorators;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.MessageManager;
import ru.churkin.homeTeco.designPatterns.lesson4.homework.model.Message;

public class MessageDecoratorBase implements MessageManager {

    protected MessageManager messageManager;

    public MessageDecoratorBase(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    @Override
    public void sendMessage(Message message) {
        Message decor = beforeSend(message);
        messageManager.sendMessage(decor);
    }

    @Override
    public Message receiveMessage(Message message) {
        return afterReceive(message);
    }

    protected Message beforeSend(Message message) {
        return message;
    }

    protected Message afterReceive(Message message){
        return message;
    }
}
