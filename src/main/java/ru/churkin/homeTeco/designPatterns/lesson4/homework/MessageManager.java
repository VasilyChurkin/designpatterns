package ru.churkin.homeTeco.designPatterns.lesson4.homework;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.model.Message;

public interface MessageManager {

    void sendMessage(Message message);

    Message receiveMessage(Message message);

}
