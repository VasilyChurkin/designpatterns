package ru.churkin.homeTeco.designPatterns.lesson4.homework;

import ru.churkin.homeTeco.designPatterns.lesson4.homework.model.Message;

public class Main {

    public static void main(String[] args) {

        MessageManager messageManager = new MessageManagerImpl();

        MessageManager manager = new MessageBuilder(messageManager)
//                .encodeUsers()
                .encodeBody()
                .build();

        Message message = new Message("author", "receiver", "text");
        System.out.println("Sending message before encode: " + message);
        manager.sendMessage(message);

    }
}
