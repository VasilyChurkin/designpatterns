package ru.churkin.homeTeco.designPatterns.lesson3.homework.model;

import java.util.Set;

public class Email {

    private String subject;
    private String author;
    private Set<String> receiver;
    private Content content;

    public Email(String subject, Set<String> receiver, String author, Content content) {
        this.subject = subject;
        this.author = author;
        this.receiver = receiver;
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String rec : receiver) {
            sb.append(rec + " ");
        }
        return "Email{" +
                "subject='" + subject + '\'' +
                ", author='" + author + '\'' +
                ", receiver= [" + String.join(  ", ", receiver) +
                "], content=" + content +
                '}';
    }
}
