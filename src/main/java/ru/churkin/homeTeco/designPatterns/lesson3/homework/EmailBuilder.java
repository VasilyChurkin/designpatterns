package ru.churkin.homeTeco.designPatterns.lesson3.homework;

import ru.churkin.homeTeco.designPatterns.lesson3.homework.model.Content;
import ru.churkin.homeTeco.designPatterns.lesson3.homework.model.Email;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class EmailBuilder {

    public FromToBuilderImpl subject(String subject) {
        return new FromToBuilderImpl(subject);
    }

    public class FromToBuilderImpl extends ToBuilderImpl implements FromBuilder {
        private String author;
        private String subject;

        public FromToBuilderImpl(String subject) {
            super(subject, "Default Author");
            this.subject = subject;
            this.author = "Default Author";
        }

        @Override
        public ToBuilder from(String author) {
            this.author = author;
            return new ToBuilderImpl(subject, author);
        }
    }

    public class ToBuilderImpl implements ToBuilder {
        private String subject;
        private String author;
        private Set<String> receivers = new HashSet<>();

        public ToBuilderImpl(String subject, String author) {
            this.subject = subject;
            this.author = author;
        }

        public ToBuilderImpl() {
        }

        @Override
        public CopyToBuilderImpl to(String receiver) {
            receivers.add(receiver);
            return new CopyToBuilderImpl(subject, author, receivers);
        }

        @Override
        public CopyToBuilderImpl toAll(String... value) {
            Collections.addAll(receivers, value);
            return new CopyToBuilderImpl(subject, author, receivers);
        }
    }

    public class CopyToBuilderImpl extends ToBuilderImpl implements CopyBuilder {
        private String subject;
        private String author;
        private Set<String> receivers;


        public CopyToBuilderImpl(String subject, String author, Set<String> receivers) {
            this.author = author;
            this.subject = subject;
            this.receivers = receivers;
        }

        @Override
        public ContentCopyBuilder copy(String receiver) {
            receivers.add(receiver);
            return new ContentCopyBuilderImpl(subject, author, receivers);
        }

        @Override
        public ContentCopyBuilder copyAll(String... value) {
            Collections.addAll(receivers, value);
            return new ContentCopyBuilderImpl(subject, author, receivers);
        }
    }

    public class ContentCopyBuilderImpl implements ContentCopyBuilder {
        private String subject;
        private String author;
        private Set<String> receivers;

        public ContentCopyBuilderImpl(String subject, String author, Set<String> receivers) {
            this.subject = subject;
            this.author = author;
            this.receivers = receivers;
        }

        @Override
        public ContentCopyBuilder copy(String receiver) {
            receivers.add(receiver);
            return new ContentCopyBuilderImpl(subject, author, receivers);
        }

        @Override
        public ContentCopyBuilder copyAll(String... value) {
            Collections.addAll(receivers, value);
            return new ContentCopyBuilderImpl(subject, author, receivers);
        }

        @Override
        public ResultBuilder content(Content content) {
            return new ResultBuilderImpl(subject, receivers, author, content);
        }
    }

    public class ResultBuilderImpl implements ResultBuilder {
        private String subject;
        private String author;
        private Set<String> receivers;
        private Content content;

        public ResultBuilderImpl(String subject, Set<String> receivers, String author, Content content) {
            this.subject = subject;
            this.author = author;
            this.receivers = receivers;
            this.content = content;
        }

        @Override
        public Email build() {
            return new Email(subject, receivers, author, content);
        }
    }

    public interface FromBuilder {
        ToBuilder from(String author);
    }

    public interface ToBuilder {
        CopyToBuilderImpl to(String receiver);
        CopyToBuilderImpl toAll(String... receivers);
    }

    public interface CopyBuilder {
        ContentCopyBuilder copy(String receiver);
        ContentCopyBuilder copyAll(String... value);
    }

    public interface ContentBuilder {
        ResultBuilder content(Content content);
    }

    public interface ContentCopyBuilder extends CopyBuilder, ContentBuilder {
    }

    public interface ResultBuilder {
        Email build();
    }


}
