package ru.churkin.homeTeco.designPatterns.lesson3.homework.model;

public class Content {

    private String signature;
    private String body;

    public Content(String body, String signature) {
        this.signature = signature;
        this.body = body;
    }

    @Override
    public String toString() {
        return "Content{" +
                "signature='" + signature + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
