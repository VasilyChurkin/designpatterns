package ru.churkin.homeTeco.designPatterns.lesson3.homework;

import ru.churkin.homeTeco.designPatterns.lesson3.homework.model.Content;
import ru.churkin.homeTeco.designPatterns.lesson3.homework.model.Email;

public class Main {
    public static void main(String[] args) {
        Email email = new EmailBuilder()
                .subject("subj")
                .from("author")
                .to("rec1")
                .copyAll("rec2", "rec3")
                .content(new Content("body", "signature"))
                .build();

        System.out.println(email);

        Email email2 = new EmailBuilder()
                .subject("subject")
                .toAll("to1", "t02", "to3")
                .copy("to6")
                .copyAll("to76", "to6")
                .copy("to9")
                .content(new Content("b", "S"))
                .build();

        System.out.println(email2);
    }
}
