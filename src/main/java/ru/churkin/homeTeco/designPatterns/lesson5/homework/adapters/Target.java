package ru.churkin.homeTeco.designPatterns.lesson5.homework.adapters;

import ru.churkin.homeTeco.designPatterns.lesson5.homework.IDbEntity;

public interface Target<T extends IDbEntity> {

    void create(T entity);

    T read(Class<T> clazz, Long id);

    void update(T entity);

    void delete(T entity);

}
