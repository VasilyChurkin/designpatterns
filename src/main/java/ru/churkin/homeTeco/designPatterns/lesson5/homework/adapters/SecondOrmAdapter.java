package ru.churkin.homeTeco.designPatterns.lesson5.homework.adapters;

import ru.churkin.homeTeco.designPatterns.lesson5.homework.DbUserEntity;
import ru.churkin.homeTeco.designPatterns.lesson5.homework.DbUserInfoEntity;
import ru.churkin.homeTeco.designPatterns.lesson5.homework.IDbEntity;
import ru.churkin.homeTeco.designPatterns.lesson5.homework.orm.second.ISecondOrm;

public class SecondOrmAdapter<T extends IDbEntity> implements Target<T> {

    private ISecondOrm secondOrm;

    public SecondOrmAdapter(ISecondOrm secondOrm) {
        this.secondOrm = secondOrm;
    }

    @Override
    public void create(T entity) {
        if (entity instanceof DbUserEntity) {
            secondOrm.getContext().getUsers().add((DbUserEntity) entity);
        }
        if (entity instanceof DbUserInfoEntity) {
            secondOrm.getContext().getUserInfos().add(((DbUserInfoEntity) entity));
        }
    }

    @Override
    public T read(Class<T> clazz, Long id) {
        if (DbUserEntity.class.equals(clazz)) {
            return (T) secondOrm.getContext().getUsers().stream()
                    .filter(user -> user.getId().equals(id))
                    .findFirst().orElse(null);
        }
        if (DbUserInfoEntity.class.equals(clazz)) {
            return (T) secondOrm.getContext().getUserInfos().stream()
                    .filter(userInfo -> userInfo.getId().equals(id))
                    .findFirst().orElse(null);
        }
        return null;
    }

    @Override
    public void update(T entity) {
        if (entity instanceof DbUserEntity) {
            secondOrm.getContext().getUsers().add((DbUserEntity) entity);
        }
        if (entity instanceof DbUserInfoEntity) {
            secondOrm.getContext().getUserInfos().add(((DbUserInfoEntity) entity));
        }
    }

    @Override
    public void delete(T entity) {
        if (entity instanceof DbUserEntity) {
            secondOrm.getContext().getUsers().remove(entity);
        }
        if (entity instanceof DbUserInfoEntity) {
            secondOrm.getContext().getUserInfos().remove(entity);
        }
    }
}
