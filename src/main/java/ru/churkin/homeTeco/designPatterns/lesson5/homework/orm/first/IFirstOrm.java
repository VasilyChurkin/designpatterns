package ru.churkin.homeTeco.designPatterns.lesson5.homework.orm.first;


import ru.churkin.homeTeco.designPatterns.lesson5.homework.IDbEntity;

/**
 * IFirstOrm.
 *
 * @author Ilya_Sukhachev
 */
public interface IFirstOrm<T extends IDbEntity> {

    void create(T entity);

    T read(int id);

    void update(T entity);

    void delete(T entity);
}
