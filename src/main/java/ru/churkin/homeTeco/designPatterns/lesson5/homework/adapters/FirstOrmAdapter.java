package ru.churkin.homeTeco.designPatterns.lesson5.homework.adapters;

import ru.churkin.homeTeco.designPatterns.lesson5.homework.IDbEntity;
import ru.churkin.homeTeco.designPatterns.lesson5.homework.orm.first.IFirstOrm;

public class FirstOrmAdapter<T extends IDbEntity> implements Target<T> {

    private IFirstOrm<T> firstOrm;

    public FirstOrmAdapter(IFirstOrm<T> firstOrm) {
        this.firstOrm = firstOrm;
    }

    @Override
    public void create(T entity) {
        firstOrm.create(entity);
    }

    @Override
    public T read(Class<T> clazz, Long id) {
        if (id > Integer.MAX_VALUE || id < Integer.MIN_VALUE) {
            throw new IllegalArgumentException("Illegal value of id for First ORM");
        }
        return firstOrm.read((int) (long) id);
    }

    @Override
    public void update(T entity) {
        firstOrm.update(entity);
    }

    @Override
    public void delete(T entity) {
        firstOrm.delete(entity);
    }

}
