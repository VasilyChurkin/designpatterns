package ru.churkin.homeTeco.designPatterns.lesson5.homework.orm.second;

/**
 * ISecondOrm.
 *
 * @author Ilya_Sukhachev
 */
public interface ISecondOrm {

    ISecondOrmContext getContext();
}
