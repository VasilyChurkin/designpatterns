package ru.churkin.homeTeco.designPatterns.lesson5.homework.orm.second;


import ru.churkin.homeTeco.designPatterns.lesson5.homework.DbUserEntity;
import ru.churkin.homeTeco.designPatterns.lesson5.homework.DbUserInfoEntity;

import java.util.Set;

/**
 * ISecondOrmContext.
 *
 * @author Ilya_Sukhachev
 */
public interface ISecondOrmContext {

    Set<DbUserEntity> getUsers();
    Set<DbUserInfoEntity> getUserInfos();

}
