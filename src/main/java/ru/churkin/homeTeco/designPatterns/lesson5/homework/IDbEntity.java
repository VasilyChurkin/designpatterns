package ru.churkin.homeTeco.designPatterns.lesson5.homework;

/**
 * IDbEntity.
 *
 * @author Ilya_Sukhachev
 */
public interface IDbEntity {

    Long getId();

    void setId(Long id);
}
