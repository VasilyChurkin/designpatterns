package ru.churkin.homeTeco.designPatterns.lesson1.homework.source;

/**
 * ILeg.
 *
 * @author Ilya_Sukhachev
 */
public interface ILeg {

    int getHeight();

    void setHeight(int height);
}
