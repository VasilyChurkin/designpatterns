package ru.churkin.homeTeco.designPatterns.lesson1.homework.source;

/**
 * Table.
 *
 * @author Ilya_Sukhachev
 */
public class Table implements IFurniture {

    private int length;
    private int width;
    private int weight;
    private Iterable<ILeg> legs;
    private Factory factory;

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public Iterable<ILeg> getLegs() {
        return legs;
    }

    @Override
    public void setLegs(Iterable<ILeg> legs) {
        this.legs = legs;
    }

    @Override
    public Factory getFactory() {
        return factory;
    }

    @Override
    public void setFactory(Factory factory) {
        this.factory = factory;
    }
}
