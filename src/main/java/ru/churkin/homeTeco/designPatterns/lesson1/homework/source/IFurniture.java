package ru.churkin.homeTeco.designPatterns.lesson1.homework.source;

/**
 * IFurniture.
 *
 * @author Ilya_Sukhachev
 */
public interface IFurniture {
    int getLength();

    void setLength(int length);

    int getWidth();

    void setWidth(int width);

    int getWeight();

    void setWeight(int weight);

    Iterable<ILeg> getLegs();

    void setLegs(Iterable<ILeg> legs);

    Factory getFactory();

    void setFactory(Factory factory);
}
