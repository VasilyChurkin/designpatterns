package ru.churkin.homeTeco.designPatterns.lesson1.homework.source;

/**
 * MetalLeg.
 *
 * @author Ilya_Sukhachev
 */
public class MetalLeg implements ILeg {

    protected int height;

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }
}
