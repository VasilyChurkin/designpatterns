package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public class Diamond implements Figure{
    int h1;
    int h2;

    public Diamond() {
    }

    public Diamond(int h1, int h2) {
        this.h1 = h1;
        this.h2 = h2;
    }

    @Override
    public void draw(DrawVisitor visitor) {
        double status = visitor.visit(this);
        System.out.println("Drawing diamond finished with status: " + status);
    }

    @Override
    public void coloring(ColorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void getSquare(SquareVisitor visitor) {
        double square = visitor.visit(this);
        System.out.println("Diamond square is : " + square);
    }

    @Override
    public void getPerimeter(PerimeterVisitor visitor) {
        double perimeter = visitor.visit(this);
        System.out.println("Diamod perimeter is : " + perimeter);
    }
}
