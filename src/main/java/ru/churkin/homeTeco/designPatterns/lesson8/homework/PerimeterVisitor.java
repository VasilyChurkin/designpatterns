package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public class PerimeterVisitor implements Visitor {
    public PerimeterVisitor() {
    }

    @Override
    public double visit(Rectangle rectangle) {
        return 2 * (rectangle.a + rectangle.b);
    }

    @Override
    public double visit(Triangle triangle) {
        return triangle.a + triangle.b + triangle.c;
    }

    @Override
    public double visit(Diamond diamond) {
        return 4 * Math.sqrt(0.25*diamond.h1*diamond.h1 + 0.25*diamond.h2*diamond.h2);
    }
}
