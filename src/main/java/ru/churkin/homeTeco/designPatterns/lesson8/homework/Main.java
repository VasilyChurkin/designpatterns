package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public class Main {

    public static void main(String[] args) {
        Figure rectangle = new Rectangle(2, 3);
        Figure triangle = new Triangle(5, 6);
        Figure diamond = new Diamond(10, 4);

        rectangle.draw(new DrawVisitor(2,3));
        triangle.draw(new DrawVisitor(5,6));
        diamond.draw(new DrawVisitor(10,4));
        System.out.println("\n==================\n");

        rectangle.getSquare(new SquareVisitor());
        triangle.getSquare(new SquareVisitor());
        diamond.getSquare(new SquareVisitor());
        System.out.println("\n==================\n");

        rectangle.getPerimeter(new PerimeterVisitor());
        triangle.getPerimeter(new PerimeterVisitor());
        diamond.getPerimeter(new PerimeterVisitor());
        System.out.println("\n==================\n");

        rectangle.coloring(new ColorVisitor("Red"));
        triangle.coloring(new ColorVisitor("Blue"));
        diamond.coloring(new ColorVisitor("Green"));
    }
}
