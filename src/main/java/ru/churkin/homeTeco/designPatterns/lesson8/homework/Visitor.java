package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public interface Visitor {
    double visit(Rectangle rectangle);
    double visit(Triangle triangle);
    double visit(Diamond diamond);
}
