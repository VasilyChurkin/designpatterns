package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public class Triangle implements Figure {
    int a;
    double b;
    double c;
    int h;

    public Triangle() {
    }

    public Triangle(int a, int h) {
        this.a = a;
        this.h = h;
    }

    @Override
    public void draw(DrawVisitor visitor) {
        double status = visitor.visit(this);
        System.out.println("Drawing rectangle finished with status: " + status);
    }

    @Override
    public void coloring(ColorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void getSquare(SquareVisitor visitor) {
        double square = visitor.visit(this);
        System.out.println("Triangle square is : " + square);
    }

    @Override
    public void getPerimeter(PerimeterVisitor visitor) {
        double perimeter = visitor.visit(this);
        System.out.println("Triangle perimeter is : " + perimeter);
    }
}
