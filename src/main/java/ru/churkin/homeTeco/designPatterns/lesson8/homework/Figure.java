package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public interface Figure {
    void getSquare(SquareVisitor visitor);
    void getPerimeter(PerimeterVisitor visitor);
    void draw(DrawVisitor visitor);

    void coloring(ColorVisitor visitor);
}
