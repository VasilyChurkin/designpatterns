package ru.churkin.homeTeco.designPatterns.lesson8.homework;

/**
 * Необходимо реализовать простейшую иерархию фигур, (Прямоугольник,
 * треугольник и любую фигуру на ваш выбор).
 * Для данной иерархии реализовать паттерн посетитель с операциями:
 * draw(int x, int y)
 * getArea()
 * getPerimetr()
 * anything(...) – опять же ваша на выбор
 */