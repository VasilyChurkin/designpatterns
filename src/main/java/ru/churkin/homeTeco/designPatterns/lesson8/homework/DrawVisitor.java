package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public class DrawVisitor implements Visitor {

    int x;
    int y;

    public DrawVisitor(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double visit(Rectangle rectangle) {
        try {
            System.out.println("Drawing rectangle in point (" + x + ", " + y + ")");
        } catch (Exception e) {
            return 1;
        }
        return 0;
    }

    @Override
    public double visit(Triangle triangle) {

        try {
            System.out.println("Drawing triangle in point (" + x + ", " + y + ")");
        } catch (Exception e) {
            return 1;
        }
        return 0;
    }

    @Override
    public double visit(Diamond diamond) {
        try {
            System.out.println("Drawing diamond in point (" + x + ", " + y + ")");
        } catch (Exception e) {
            return 1;
        }
        return 0;
    }
}
