package ru.churkin.homeTeco.designPatterns.lesson8.homework;

public class SquareVisitor implements Visitor {
    public SquareVisitor() {
    }

    @Override
    public double visit(Rectangle rectangle) {
        return rectangle.a * rectangle.b;
    }

    @Override
    public double visit(Triangle triangle) {
        return 0.5 * triangle.a * triangle.h;
    }

    @Override
    public double visit(Diamond diamond) {
        return diamond.h1 * diamond.h2;
    }
}
